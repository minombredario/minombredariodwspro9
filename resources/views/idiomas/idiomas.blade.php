@extends('layouts.layout')

@section('content')
    <h3>Idiomas</h3>
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('idiomas.create')}}" class="btn btn-info pull-right" data-toggle="tooltip" title="Añadir idioma"><span class="glyphicon glyphicon-plus"></span></a>
        </div>

    </div>
    <table class="table table-striped">
        <thead>
        <th>ID</th>
        <th>Nombre</th>
        <th>Horas</th>
        <th>Acción</th>
    </thead>
    <tbody>
        @foreach ($idiomas as $idioma)
            <tr>
                <td>{{ $idioma->id }}</td>
                <td>{{ $idioma->nombre }}</td>
                <td>{{ $idioma->horas }}</td>
                <td>
                    @if (count($idiomas) != 0)
                        <a href="{{route('idiomas.edit', $idioma->id)}}" class="btn btn-warning" data-toggle="tooltip" title="Editar Idioma"><span class="glyphicon glyphicon-edit"></span></a>
                        <span data-toggle="modal" data-target="#borrar-idioma-{{ $idioma->id }}">
                            <a href="#borrar-idioma-{{ $idioma->id }}" data-toggle="tooltip" title="Borrar Idioma" class="btn btn-danger">
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        </span>
                        <div class="modal fade" id="borrar-idioma-{{ $idioma->id }}" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Borrar idioma «{{ $idioma->nombre }}»</h4>
                                    </div>
                                    <div class="modal-body">
                                        ¿Quieres borrar este idioma? Esta acción no puede deshacerse, y <span class="text-danger">también eliminará a aquellos alumnos asociados al idioma</span>.
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{route('idiomas.destroy', $idioma->id)}}" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </td>
            </tr>
        @endforeach

    </tbody>
    </table>
    @if (count($idiomas) > 1)
        <div class="pull-right">{{ $idiomas->links() }}</div>
    @endif
    <br/>
    <a href="{{ url('/home') }}"><button class="btn btn-info btn-lm" >Atrás</button></a>

@endsection
