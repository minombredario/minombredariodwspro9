<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $tabla = 'alumnos';
    protected $fillable = ['nombre','idioma_id'];
    public $timestamps = false;
    
    public function Idioma() {
        return $this->belongsTo('App\Idioma', 'idioma_id');
    }
}
