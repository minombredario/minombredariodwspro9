<?php

use Illuminate\Database\Seeder;

class IdiomasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('idiomas')->insert([
        	['nombre' => 'Castellano', 	'horas' => 100],
        	['nombre' => 'Latín', 		'horas' => 110],
        	['nombre' => 'Ingles', 		'horas' => 120],
        	['nombre' => 'Aleman', 		'horas' => 130],
        	['nombre' => 'Valenciano', 	'horas' => 140],
        	['nombre' => 'Ruso', 		'horas' => 150],
        	['nombre' => 'Chino', 		'horas' => 160],
        	['nombre' => 'Francés', 	'horas' => 170],
        	['nombre' => 'Italinao', 	'horas' => 180],
        	['nombre' => 'Rumano', 		'horas' => 190]
        ]);
    }
}
