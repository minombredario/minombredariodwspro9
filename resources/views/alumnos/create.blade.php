@extends('layouts.layout')

@section('titulo', 'Crear alumno')

@section('content')

<h3><span class="glyphicon glyphicon-user"></span> Añadir alumno</h3>

    <form action="{{ route('alumnos.store') }}" method="get">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">

            <input name="id" type="hidden" class="form-control" readonly="readonly" value="{{ $id }}" />

        </div>

        <div class="form-group">

            <label name="nombre">Nombre</label>

            <input name="nombre" type="text" class="form-control" placeholder="Nombre del alumno" value="" required/>

        </div>

        <div class="form-group">

            <label name="idioma_id">Idioma</label>

            <select name="idioma_id" class="form-control">

                <option value="0">-- Escoge un Idioma --</option>

                @foreach ($idiomas as $idioma)

                    <option value="{{ $idioma->id }}" required>

                        {{ $idioma->nombre }}


                    </option>

                @endforeach;

            </select>

    </div>

        <div class="form-group">

            <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span></button>

        </div>

    </form>
<br/>

    <a href="{{ url('/alumnos') }}"><button class="btn btn-info btn-lm" >Atrás</button></a>
@endsection
