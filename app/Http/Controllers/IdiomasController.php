<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Idioma;

class IdiomasController extends Controller
{
    public function index(){

        $idiomas = DB::table('idiomas')->paginate(5);

        return view('idiomas.idiomas')->with('idiomas',$idiomas);

    }


    public function create()
    {
        $id = $this->lastId();

        return view('idiomas.create')->with('id',$id);
    }


    public function store(Request $request){

        $idioma = new Idioma($request->all());
        $idioma->save();
        return redirect()->route('idiomas');
    }


    public function edit($id){

        $idiomas = Idioma::find($id);

        return view('idiomas.edit')->with('idioma',$idiomas);
    }


    public function update(Request $request, $id){

        if (Idioma::find($id)) {

            $idioma  = Idioma::find($id);

            $idioma->nombre = $request->nombre;

            $idioma->horas = $request->horas;

            $idioma->save();

        } else {

        }

        return redirect()->route('idiomas');
    }


    public function destroy($id){

        $idioma = Idioma::find($id);

        $idioma->delete();

        return redirect()->route('idiomas');
    }

    public function lastId(){

        $id = DB::select('SELECT MAX(id) AS id FROM idiomas');

        $id = $id[0]->id;

        $id++;

        return $id;
    }
}
