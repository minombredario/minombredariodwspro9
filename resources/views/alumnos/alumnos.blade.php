@extends('layouts.layout')

@section('content')
<h3>Alumnos</h3>
        <div class="row">
            <div class="col-md-12">
                <a href="{{route('alumnos.create')}}" class="btn btn-info pull-right" data-toggle="tooltip" title="Añadir alumno"><span class="glyphicon glyphicon-plus"></span></a>
            </div>
            <div class="col-md-6">

            </div>
        </div>
    <table class="table table-striped">
        <thead>
            <th>ID</th>
            <th>Nombre</th>
            <th>Idioma</th>
            <th>Acción</th>
        </thead>
        <tbody>

        @foreach ($alumnos as $alumno)
            <tr>
                <td>{{ $alumno->id }}</td>
                <td>{{ $alumno->nombre }}</td>
                <td>{{ $alumno->idioma->nombre }}</td>
            <td>
                    @if (count($alumnos) != 0)
                        <a href="{{route('alumnos.edit', $alumno->id)}}" class="btn btn-warning" data-toggle="tooltip" title="Editar alumno"><span class="glyphicon glyphicon-edit"></span></a>
                        <span data-toggle="modal" data-target="#borrar-alumno-{{ $alumno->id }}">
                            <a href="#borrar-alumno-{{ $alumno->id }}" data-toggle="tooltip" title="Borrar alumno" class="btn btn-danger">
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        </span>
                        <div class="modal fade" id="borrar-alumno-{{ $alumno->id }}" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Borrar alumno «{{ $alumno->nombre }}»</h4>
                                    </div>
                                    <div class="modal-body">
                                        ¿Quieres borrar este alumno? Esta acción no puede deshacerse.
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{route('alumnos.destroy', $alumno->id)}}" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @if (count($alumnos) > 1)
        <div class="pull-right">{{ $alumnos->links() }}</div>
    @endif
    <br/>
    <a href="{{ url('/home') }}"><button class="btn btn-info btn-lm" >Atrás</button></a>

@endsection


