@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                 <div class="panel-heading">Menú Principal</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{route('idiomas')}}"><button class="btn btn-primary btn-lg btn-block">Gestión de Idiomas</button></a>
                        </div>
                        <div class="col-sm-6">
                            <a href="{{route('alumnos')}}"><button class="btn btn-primary btn-lg btn-block">Gestión de Alumnos</button></a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a href="../public/documentacion/documentacion.pdf"><button class="btn btn-info btn-lg btn-block">Documentación</button></a>
            
        </div>
    </div>

    
@endsection
