<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Alumno;

use App\Idioma;

class AlumnosController extends Controller{
    

    public function index() {
        
        $alumnos = Alumno::paginate(5);

        return view('alumnos.alumnos')->with('alumnos',$alumnos);

    }

    public function create() {

        $id = $this->lastId();
        $idiomas = Idioma::all();
        //dd($idiomas);
        return view('alumnos.create')->with('id',$id)->with('idiomas',$idiomas);

    }

    public function store(Request $request) {

        $alumno = new Alumno($request->all());

        $alumno->save();

        return redirect()->route('alumnos');

    }

    public function edit($id) {

        $alumno = Alumno::find($id);
        
        $idiomas = Idioma::all();

        return view('alumnos.edit')->with('alumno',$alumno)->with('idioma',$idiomas);

    }

    public function update(Request $request, $id) {

         if (Alumno::find($id)) {

            $alumno = Alumno::find($id);

            $alumno->nombre = $request->nombre;

            $alumno->idioma_id = $request->idioma;

            $alumno->save();


        } else {



        }

        return redirect()->route('alumnos');

    }

    public function destroy($id){

        $alumno = Alumno::find($id);

        $alumno->delete();

        return redirect()->route('alumnos');

    }
   

    public function lastId(){

        $id = DB::select('SELECT MAX(id) AS id FROM alumnos');

        $id = $id[0]->id;

        $id++;

        return $id;
    }

}
