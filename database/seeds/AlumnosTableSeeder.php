<?php

use Illuminate\Database\Seeder;

class AlumnosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Alumnos')->insert([
        	['nombre' => 'Álan',        'idioma_id' => 1],
        	['nombre' => 'Jacinto',     'idioma_id' => 2],
        	['nombre' => 'Jesús',       'idioma_id' => 3],
        	['nombre' => 'Belén',       'idioma_id' => 4],
        	['nombre' => 'Gaspar',      'idioma_id' => 5],
        	['nombre' => 'Gustavo',     'idioma_id' => 6],
        	['nombre' => 'Luis',        'idioma_id' => 7],
        	['nombre' => 'Jose',        'idioma_id' => 8],
        	['nombre' => 'María',       'idioma_id' => 9],
        	['nombre' => 'Eva',         'idioma_id' => 10],
        	['nombre' => 'Mónica',      'idioma_id' => 1],
        	['nombre' => 'Miguel',      'idioma_id' => 2],
        	['nombre' => 'Tomás',       'idioma_id' => 3],
        	['nombre' => 'Victor',      'idioma_id' => 4],
        	['nombre' => 'Rodrigo',     'idioma_id' => 5],
        	['nombre' => 'Victor',      'idioma_id' => 6],
        	['nombre' => 'Paula',       'idioma_id' => 7],
        	['nombre' => 'Aroa',        'idioma_id' => 8],
        	['nombre' => 'Marta',       'idioma_id' => 9],
        	['nombre' => 'Mª Carmen',   'idioma_id' => 10]
        ]);
    }
}
