@extends('layouts.layout')

@section('content')

    <h3><span class="glyphicon glyphicon-pencil"></span> Editar alumno «{{ $alumno->nombre }}»</h3>

    <form action="{{ route('alumnos.update', $alumno->id) }}" method="POST">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">

             <input name="id" type="hidden" class="form-control" readonly="readonly" value="{{ $alumno->id }}" />

        </div>

        <div class="form-group">

            <label name="nombre">Nombre</label>

            <input name="nombre" type="text" class="form-control" placeholder="Título del idioma" value="{{ $alumno->nombre }}" />

        </div>

        <div class="form-group">

            <label name="idioma">Idioma</label>

            <select name="idioma" class="form-control">

                <option value="0">-- Escoge un Idioma --</option>

                @foreach ($idioma as $idioma)

                    @if ($alumno->idioma_id == $idioma->id)

                        <option value="{{ $idioma->id }}" selected>

                    @else

                        <option value="{{ $idioma->id }}">

                    @endif

                        {{ $idioma->nombre }}

                        </option>

                @endforeach;

            </select>

    </div>


        <div class="form-group">

            <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span></button>

        </div>

    </form>

    <br/>

    <a href="{{ url('/alumnos') }}"><button class="btn btn-info btn-lm" >Atrás</button></a>


@endsection
