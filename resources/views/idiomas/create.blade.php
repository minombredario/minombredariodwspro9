@extends('layouts.layout')

@section('titulo', 'Crear Idioma')


@section('content')
    <h3><span class="glyphicon glyphicon-blackboard"></span> Añadir nuevo Idioma</h3>

    <form action="{{route('idiomas.store')}}" method="get">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <input name="id" type="hidden" class="form-control" readonly="readonly" value="{{ $id }}" />
        </div>

        <div class="form-group">
            <label name="nombre">Nombre</label>
            <input name="nombre" type="text" class="form-control" placeholder="Idioma" />
        </div>

        <div class="form-group">
            <label name="horas">Horas</label>
            <input name="horas" type="text" class="form-control" placeholder="Horas totales" />
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span></button>
        </div>
    </form>
<br/>
    <a href="{{ url('/idiomas') }}"><button class="btn btn-info btn-lm" >Atrás</button></a>
@endsection
