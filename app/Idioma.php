<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idioma extends Model
{
    protected $tabla = 'idiomas';
    protected $fillable = ['nombre','horas'];
    public $timestamps = true;

    public function alumnos() {
        return $this->hasMany('App\Alumno', 'id');
    }
}
