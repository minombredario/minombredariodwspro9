<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('alumnos', 'AlumnosController');

//Rutas de alumnos
Route::get('alumnos', [
    'as' => 'alumnos',
    'uses' => 'AlumnosController@index'
])->middleware("auth");;

Route::get('alumnos/borrar/{id}', [
    'as' => 'alumnos.destroy',
    'uses' => 'AlumnosController@destroy'
])->middleware("auth");

Route::get('alumnos/editar/{id}', [
    'as' => 'alumnos.edit',
    'uses' => 'AlumnosController@edit'
])->middleware("auth");

Route::get('alumnos/crear', [
    'as' => 'alumnos.create',
    'uses' => 'AlumnosController@create'
])->middleware("auth");

Route::get('alumnos/guardar', [
    'as' => 'alumnos.store',
    'uses' => 'AlumnosController@store'
])->middleware("auth");

Route::POST('alumnos/actualizar/{id}', [
    'as' => 'alumnos.update',
    'uses' => 'AlumnosController@update'
])->middleware("auth");;

//Rutas de Idiomas

//Route::resource('idiomas', 'IdiomasController');

Route::get('idiomas', [
    'as' => 'idiomas',
    'uses' => 'IdiomasController@index'
]);

Route::get('idiomas/borrar/{id}', [
    'as' => 'idiomas.destroy',
    'uses' => 'IdiomasController@destroy'
])->middleware("auth");

Route::get('idiomas/editar/{id}', [
    'as' => 'idiomas.edit',
    'uses' => 'IdiomasController@edit'
])->middleware("auth");

Route::get('idiomas/crear', [
    'as' => 'idiomas.create',
    'uses' => 'IdiomasController@create'
])->middleware("auth");


Route::get('idiomas/guardar', [
    'as' => 'idiomas.store',
    'uses' => 'IdiomasController@store'
])->middleware("auth");

Route::POST('idiomas/actualizar/{id}', [
    'as' => 'idiomas.update',
    'uses' => 'IdiomasController@update'
])->middleware("auth");

//Rutas autenticador

Auth::routes();

Route::get('/home', 'HomeController@index');


//descarga doc

Route::get('download', function() {
    return Response::download(Input::get('path'));
});