@extends('layouts.layout')

@section('titulo', 'Editar idioma')


@section('content')

    <h3><span class="glyphicon glyphicon-pencil"></span> Editar idioma «{{ $idioma->nombre }}»</h3>

    <form action="{{ route('idiomas.update', $idioma->id) }}" method="POST">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">

            <input name="id" type="hidden" class="form-control" readonly="readonly" value="{{ $idioma->id }}" />

        </div>

        <div class="form-group">

            <label name="nombre">Nombre</label>

            <input name="nombre" type="text" class="form-control" placeholder="Título del idioma" value="{{ $idioma->nombre }}" />

        </div>

        <div class="form-group">

            <label name="horas">Horas</label>

            <input name="horas" type="text" class="form-control" placeholder="Horas totales" value="{{ $idioma->horas }}" />

        </div>

        <div class="form-group">

            <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span></button>

        </div>

    </form>

<br/>
    <a href="{{ url('/idiomas') }}"><button class="btn btn-info btn-lm" >Atrás</button></a>
@endsection
